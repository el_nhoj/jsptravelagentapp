
/**
 * AirlineServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package com.rmit.service.stubs;

    /**
     *  AirlineServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class AirlineServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public AirlineServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public AirlineServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for login method
            * override this method for handling normal response from login operation
            */
           public void receiveResultlogin(
                    com.rmit.service.stubs.AirlineServiceStub.TravelAgent result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from login operation
           */
            public void receiveErrorlogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for confirmBooking method
            * override this method for handling normal response from confirmBooking operation
            */
           public void receiveResultconfirmBooking(
                    com.rmit.service.stubs.AirlineServiceStub.BookingConfirmationOrder result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from confirmBooking operation
           */
            public void receiveErrorconfirmBooking(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for bookFlight method
            * override this method for handling normal response from bookFlight operation
            */
           public void receiveResultbookFlight(
                    com.rmit.service.stubs.AirlineServiceStub.BookingConfirmationAirline result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from bookFlight operation
           */
            public void receiveErrorbookFlight(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createCustomer method
            * override this method for handling normal response from createCustomer operation
            */
           public void receiveResultcreateCustomer(
                    com.rmit.service.stubs.AirlineServiceStub.Customer result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createCustomer operation
           */
            public void receiveErrorcreateCustomer(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFlights method
            * override this method for handling normal response from getFlights operation
            */
           public void receiveResultgetFlights(
                    com.rmit.service.stubs.AirlineServiceStub.Flight[] result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFlights operation
           */
            public void receiveErrorgetFlights(java.lang.Exception e) {
            }
                


    }
    