package com.rmit.model.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.log4j.Logger;

import com.rmit.model.beans.Accommodation;
import com.rmit.model.beans.BookingConfirmationOrder;
import com.rmit.model.beans.Customer;
import com.rmit.model.beans.DataBindingUtils;
import com.rmit.model.beans.Flight;
import com.rmit.service.stubs.AccommodationServiceStub;
import com.rmit.service.stubs.AirlineServiceStub;

public class ApplicationDatabase {
	private final Logger logger = Logger.getLogger("dbConnection");
	private String driver = "org.apache.derby.jdbc.ClientDriver";
	private String protocol = "jdbc:derby://localhost:1527/";
	private Connection con;
	private Statement st;
	private AirlineServiceStub airStub;
	private AccommodationServiceStub accommStub;
	private ConfigurationContext ctx;

	public ApplicationDatabase() {
	}

	// Method to check if user is valid
	public boolean validateUser(String username, String password) {
		boolean match = false;
		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB;create=true");
			st = con.createStatement();

			ResultSet rs = st.executeQuery("SELECT * FROM Customer");

			// Loop through result set and try to find a match
			while (rs.next()) {
				String user = rs.getString(3);
				String pass = rs.getString(4);

				if (user.equals(username) && pass.equals(password)) {
					match = true;
				}
			}

			// Release resources
			rs.close();
			st.close();
			con.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
		return match;
	}

	// Method that calls AirlineServiceStub and returns an array of flights
	public Flight[] getFlights() {
		try {
			airStub = new AirlineServiceStub(ctx, 
					"http://localhost:8080/axis2/services/AirlineService");

			AirlineServiceStub.Flight[] stubFlights = airStub.getFlights();
			Flight[] flights = new Flight[stubFlights.length];
			for (int i = 0; i < stubFlights.length; i++) {
				flights[i] = DataBindingUtils.toSOAFlight(stubFlights[i]);
			}
			airStub = null;
			return flights;

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	// Method that calls AccommodationServiceStub and returns an array of
	// accommodations
	public Accommodation[] getAccommodation(String destination) {
		try {
			ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(
							"D:/Documents/Java/3401071_ws2",
							"D:/Documents/Java/3401071_ws2/client.axis2.xml");
			accommStub = new AccommodationServiceStub(ctx, 
					"http://localhost:28080/axis2/services/AccommodationService");

			AccommodationServiceStub.Accommodation[] stubAccommodations = accommStub
					.getAccommodationList(destination);
			Accommodation[] accommodations = new Accommodation[stubAccommodations.length];
			for (int i = 0; i < stubAccommodations.length; i++) {
				accommodations[i] = DataBindingUtils
						.toSOAAccommodation(stubAccommodations[i]);
			}
			accommStub = null;
			return accommodations;

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// Get ALL accommodations
	public Accommodation[] getAccommodation() {
		try {
			accommStub = new AccommodationServiceStub(
					"http://localhost:8080/axis2/services/AccommodationService");

			AccommodationServiceStub.Accommodation[] stubAccommodations = accommStub
					.getAllAccommodationList();
			Accommodation[] accommodations = new Accommodation[stubAccommodations.length];
			for (int i = 0; i < stubAccommodations.length; i++) {
				accommodations[i] = DataBindingUtils
						.toSOAAccommodation(stubAccommodations[i]);
			}
			accommStub = null;
			return accommodations;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// Method to return an array of destinations
	public String[] getDestination() {
		String[] destinationList = null;
		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB;create=true");
			st = con.createStatement();

			// Find out how many rows exist in table
			ResultSet rs = st
					.executeQuery("SELECT COUNT(*) AS rowcount FROM Flight");
			rs.next();
			int size = rs.getInt("rowcount");

			// Instantiate array with the row size;
			destinationList = new String[size];

			ResultSet rs2 = st.executeQuery("SELECT destination FROM Flight");
			int n = 0;
			// Loop through result set and add destinations to array
			while (rs2.next()) {
				destinationList[n] = rs2.getString(1).toString();
				n++;
			}

			// Release resources
			rs.close();
			rs2.close();
			st.close();
			con.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
		return destinationList;
	}

	public Customer getCustomerDetails(String un) {
		Customer customer = null;
		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB;create=true");
			st = con.createStatement();

			// Query and display results
			ResultSet rs = st
					.executeQuery("SELECT * FROM Customer WHERE username='"
							+ un + "'");

			// Get customer details and create customer object
			while (rs.next()) {
				customer = new Customer();
				customer.setId(rs.getString(1).toString());
				customer.setName(rs.getString(2).toString());
				customer.setUsername(rs.getString(3).toString());
				customer.setPassword(rs.getString(4).toString());
				customer.setAddress(rs.getString(5).toString());
				customer.setBalance(rs.getDouble(6));
			}
			// Release resources
			rs.close();
			st.close();
			con.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}

		return customer;
	}

	// Method to create booking confirmation for flights
	public BookingConfirmationOrder createBookingConfirmation(
			Customer customer, double total, String cartContents) {
		BookingConfirmationOrder bco = null;

		try {
			airStub = new AirlineServiceStub(
					"http://localhost:8080/axis2/services/AirlineService");

			// Convert customer into stub object
			AirlineServiceStub.Customer stubCustomer = DataBindingUtils
					.toAirlineCustomer(customer);

			// Attempt to create a booking confirmation
			AirlineServiceStub.BookingConfirmationOrder stubBook = airStub
					.confirmBooking(stubCustomer, total, cartContents);

			if (stubBook != null) {
				// Convert back into a useable bean object and return it
				bco = DataBindingUtils.toSOABookingConfirmationOrder(stubBook);
			}

		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
		return bco;
	}

	// Method to retrieve booking confirmation from database
	public BookingConfirmationOrder getBookingConfirmation(String bookingId) {
		BookingConfirmationOrder bco = null;

		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB;create=true");
			st = con.createStatement();

			// Query and display results
			ResultSet rs = st
					.executeQuery("SELECT * FROM BookingConfirmationOrder WHERE id='"
							+ bookingId + "'");

			// Get customer details and create customer object if resultSet
			// returns a match
			while (rs.next()) {
				bco = new BookingConfirmationOrder();
				bco.setId(rs.getString(1).toString());
				bco.setCartContents(rs.getString(2).toString());
				bco.setCustomerId(rs.getString(3).toString());
			}

			// Release resources
			rs.close();
			st.close();
			con.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}

		return bco;
	}
}
