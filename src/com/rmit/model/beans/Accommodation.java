package com.rmit.model.beans;

import java.io.Serializable;

public class Accommodation implements Serializable {
	
	private static final long serialVersionUID = -4332497073303476555L;
	private String id;
	private String name;
	private String address;
	private String phoneNumber;
	private String location;
	private double cost;

	public Accommodation(String id, String name, String address,
			String phoneNumber, String location, double cost) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.location = location;
		this.cost = cost;
	}

	public Accommodation() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public double getCost(){
		return cost;
	}
	
	public void setCost(double cost){
		this.cost = cost;
	}
	
	@Override
	public String toString() {
		return id + ":" + name + ":" + address + ":" + phoneNumber + ":"
				+ location + ":" + cost;
	}
	
	@Override
	// Compares accommodation IDs and returns false if they are the same
    public boolean equals(Object o) {
        if (o instanceof Accommodation == false) {
            return false;
        }
        Accommodation rhs = (Accommodation) o;
        if (this.getId() != null && rhs.getId() != null) {
            return this.getId().equals(rhs.getId());
        } else {
            return false;
        }
    }
}
