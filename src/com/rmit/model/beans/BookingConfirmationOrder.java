package com.rmit.model.beans;

public class BookingConfirmationOrder {
	private String id;
	private String cartContents;
	private String customerId;

	public BookingConfirmationOrder(String id, String cartContents, String customerId) {
		this.id = id;
		this.cartContents = cartContents;
		this.customerId = customerId;
	}

	public BookingConfirmationOrder() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCartContents() {
		return cartContents;
	}

	public void setCartContents(String cartContents) {
		this.cartContents = cartContents;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
