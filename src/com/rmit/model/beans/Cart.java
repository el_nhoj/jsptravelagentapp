package com.rmit.model.beans;

import java.util.ArrayList;
import java.util.List;

public class Cart {
	private List<Flight> flights;
	private List<Accommodation> accommodations;
	private double total;

	public Cart() {
		flights = new ArrayList<Flight>();
		accommodations = new ArrayList<Accommodation>();
	}

	public List<Flight> getFlightList(){
		return flights;
	}
	
	public void setFlightList(List<Flight> flights)
	{
		this.flights = flights;
	}
	
	public List<Accommodation> getAccommodationList(){
		return accommodations;
	}
	
	public void setAccommodationList(List<Accommodation> accommodations)
	{
		this.accommodations = accommodations;
	}
	
	// Method to calculate a cart's total cost
	public double calculateTotal() {
		total = 0;
		
		if(!flights.isEmpty()){
			for(Flight f : flights){
				total += f.getCost(); 
			}
		} 
		
		if(!accommodations.isEmpty())
		{
			for(Accommodation a : accommodations)
			{
				total += a.getCost();
			}
		}
		return total;
	}
}
