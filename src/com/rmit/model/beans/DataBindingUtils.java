package com.rmit.model.beans;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.beanutils.BeanUtils;

import com.rmit.service.stubs.AccommodationServiceStub;
import com.rmit.service.stubs.AccommodationServiceStub.BookingConfirmationAccommodation;
// import stubs.ActivitiesServiceStub;
import com.rmit.service.stubs.AirlineServiceStub;
import com.rmit.service.stubs.AirlineServiceStub.BookingConfirmationAirline;

// Convert between the various paramater types used by adb framework
// makes sense since these would all be from different business and we expect to convert
public class DataBindingUtils {
	// Customer Conversions
	public static com.rmit.model.beans.Customer toSOACustomer(
			BookingConfirmationAirline bookingConfirmation) {
		com.rmit.model.beans.Customer customer = new com.rmit.model.beans.Customer();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(customer,
					bookingConfirmation.getCustomer());
		} catch (Exception e) {
			logError(e);
		}
		assert customer != null : "data binding error with Customer bean";
		return customer;
	}

	public static com.rmit.model.beans.Customer toSOACustomer(
			BookingConfirmationAccommodation bookingConfirmation) {
		com.rmit.model.beans.Customer customer = new com.rmit.model.beans.Customer();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(customer,
					bookingConfirmation.getCustomer());
		} catch (Exception e) {
			logError(e);
		}
		assert customer != null : "data binding error with Customer bean";
		return customer;
	}

	public static AirlineServiceStub.Customer toAirlineCustomer(
			com.rmit.model.beans.Customer customer) {
		AirlineServiceStub.Customer airlineCustomer = new AirlineServiceStub.Customer();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(airlineCustomer, customer);
		} catch (Exception e) {
			logError(e);
		}
		assert airlineCustomer != null : "data binding error with Customer bean";
		return airlineCustomer;
	}

	public static AccommodationServiceStub.Customer toAccommodationCustomer(
			com.rmit.model.beans.Customer customer) {
		AccommodationServiceStub.Customer accommodationCustomer = new AccommodationServiceStub.Customer();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(accommodationCustomer, customer);
		} catch (Exception e) {
			logError(e);
		}
		assert accommodationCustomer != null : "data binding error with Customer bean";
		return accommodationCustomer;
	}

	// Flight Conversions
	public static com.rmit.model.beans.Flight toSOAFlight(
			AirlineServiceStub.Flight airlineFlight) {
		com.rmit.model.beans.Flight flight = new com.rmit.model.beans.Flight();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(flight, airlineFlight);
		} catch (Exception e) {
			logError(e);
		}
		assert flight != null : "data binding error with Flight bean";
		return flight;
	}

	public static AirlineServiceStub.Flight toAirlineFlight(
			com.rmit.model.beans.Flight flight) {
		AirlineServiceStub.Flight airlineFlight = new AirlineServiceStub.Flight();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(airlineFlight, flight);
		} catch (Exception e) {
			logError(e);
		}
		assert airlineFlight != null : "data binding error with Flight bean";
		return airlineFlight;
	}

	// Accommodation Conversions
	// Accommodation Conversions
	public static com.rmit.model.beans.Accommodation toSOAAccommodation(
			AccommodationServiceStub.Accommodation accommodationAccommodation) {
		com.rmit.model.beans.Accommodation accommodation = new com.rmit.model.beans.Accommodation();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(accommodation, accommodationAccommodation);
		} catch (Exception e) {
			logError(e);
		}
		assert accommodation != null : "data binding error with Accommodation bean";
		return accommodation;
	}

	public static AccommodationServiceStub.Accommodation toAccommodationAccommodation(
			com.rmit.model.beans.Accommodation accommodation) {
		AccommodationServiceStub.Accommodation accommodationAcccommodation = new AccommodationServiceStub.Accommodation();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils
					.copyProperties(accommodationAcccommodation, accommodation);
		} catch (Exception e) {
			logError(e);
		}
		assert accommodationAcccommodation != null : "data binding error with Accommodation bean";
		return accommodationAcccommodation;
	}

	// Booking Confirmation Conversions
	// Booking Confirmation Conversions
	public static com.rmit.model.beans.BookingConfirmationAirline toSOABookingConfirmation(
			AirlineServiceStub.BookingConfirmationAirline airlineBookingConfirmation) {
		com.rmit.model.beans.BookingConfirmationAirline bookingConfirmation = new com.rmit.model.beans.BookingConfirmationAirline();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			// BeanUtils.copyProperties(bookingConfirmation,
			// airlineBookingConfirmation);
			// String a =
			// airlineBookingConfirmation.getFlight().getAirlineName();
			bookingConfirmation.setId(airlineBookingConfirmation.getId());
			bookingConfirmation.setFlight(airlineBookingConfirmation
					.getFlight());
			bookingConfirmation.setCustomer(airlineBookingConfirmation);
		} catch (Exception e) {
			logError(e);
		}
		assert bookingConfirmation != null : "data binding error with Booking confirmation bean";
		return bookingConfirmation;
	}

	public static com.rmit.model.beans.BookingConfirmationAccommodation toSOABookingConfirmationAccommodation(
			AccommodationServiceStub.BookingConfirmationAccommodation accommodationBookingConfirmation) {
		com.rmit.model.beans.BookingConfirmationAccommodation bookingConfirmation = new com.rmit.model.beans.BookingConfirmationAccommodation();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			// BeanUtils.copyProperties(bookingConfirmation,
			// accommodationBookingConfirmation);
			// String a =
			// accommodationBookingConfirmation.getAccommodation().getName();
			bookingConfirmation.setId(accommodationBookingConfirmation.getId());
			bookingConfirmation
					.setAccommodation(accommodationBookingConfirmation
							.getAccommodation());
			bookingConfirmation.setCustomer(accommodationBookingConfirmation);
		} catch (Exception e) {
			logError(e);
		}
		assert bookingConfirmation != null : "data binding error with Booking confirmation bean";
		return bookingConfirmation;
	}

	// Travel Agent Conversions
	// Travel Agent Conversions
	public static TravelAgent toSOATravelAgent(
			AirlineServiceStub.TravelAgent airlineTravelAgent) {
		com.rmit.model.beans.TravelAgent travelAgent = new com.rmit.model.beans.TravelAgent();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(travelAgent, airlineTravelAgent);
		} catch (Exception e) {
			logError(e);
		}
		assert travelAgent != null : "data binding error with Travel Agent bean";
		return travelAgent;
	}

	public static TravelAgent toSOATravelAgent(
			AccommodationServiceStub.TravelAgent accommodationTravelAgent) {
		com.rmit.model.beans.TravelAgent travelAgent = new com.rmit.model.beans.TravelAgent();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(travelAgent, accommodationTravelAgent);
		} catch (Exception e) {
			logError(e);
		}
		assert travelAgent != null : "data binding error with Travel Agent bean";
		return travelAgent;
	}

	public static AirlineServiceStub.TravelAgent toAirlineTravelAgent(
			com.rmit.model.beans.TravelAgent travelAgent) {
		AirlineServiceStub.TravelAgent airlineTravelAgent = new AirlineServiceStub.TravelAgent();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(airlineTravelAgent, travelAgent);
		} catch (Exception e) {
			logError(e);
		}
		assert airlineTravelAgent != null : "data binding error with Flight bean";
		return airlineTravelAgent;
	}

	public static AccommodationServiceStub.TravelAgent toAccommodationTravelAgent(
			com.rmit.model.beans.TravelAgent travelAgent) {
		AccommodationServiceStub.TravelAgent accommodationTravelAgent = new AccommodationServiceStub.TravelAgent();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(accommodationTravelAgent, travelAgent);
		} catch (Exception e) {
			logError(e);
		}
		assert accommodationTravelAgent != null : "data binding error with Flight bean";
		return accommodationTravelAgent;
	}

	// New method to convert to new BookingConfirmationOrder bean
	public static com.rmit.model.beans.BookingConfirmationOrder toSOABookingConfirmationOrder(
			AirlineServiceStub.BookingConfirmationOrder stubBookingConfirmationOrder) {
		com.rmit.model.beans.BookingConfirmationOrder bookingConfirmationOrder = new BookingConfirmationOrder();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(bookingConfirmationOrder, stubBookingConfirmationOrder);
		} catch (Exception e) {
			logError(e);
		}
		assert bookingConfirmationOrder != null : "data binding error with BookingConfirmationOrder bean";
		return bookingConfirmationOrder;
	}

	public static void logError(Exception e) {
		// Use built in Java logger
		Logger logger = Logger.getLogger("assignment1");
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.log(Level.SEVERE, exceptionAsString);
	}
}
