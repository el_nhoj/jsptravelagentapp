package com.rmit.model.customtag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class AccommodationCustomTag extends TagSupport {
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String address;
	private String phoneNumber;
	private String location;
	private double cost;

	public AccommodationCustomTag() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public double getCost(){
		return cost;
	}
	
	public void setCost(double cost){
		this.cost = cost;
	}
	
	@Override
	public int doStartTag() throws JspException {
		
		try{
			pageContext.setAttribute("id", this.getId());
			pageContext.setAttribute("name", this.getName());
			pageContext.setAttribute("address", this.getAddress());
			pageContext.setAttribute("phoneNumber", this.getPhoneNumber());
			pageContext.setAttribute("location", this.getLocation());
			pageContext.setAttribute("cost", this.getCost());
		}catch(Exception e){
			e.printStackTrace();
		}
		return EVAL_BODY_INCLUDE;
	}

}
