package com.rmit.model.customtag;

import java.util.Date;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class FlightCustomTag extends TagSupport {

	private static final long serialVersionUID = 1L;
	private String id;
	private String airlineName;
	private Date departureDate;
	private String departureGate;
	private String destination;
	private double cost;

	public FlightCustomTag() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureGate() {
		return departureGate;
	}

	public void setDepartureGate(String departureGate) {
		this.departureGate = departureGate;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	@Override
	public int doStartTag() throws JspException {

		try {
			pageContext.setAttribute("id", this.getId());
			pageContext.setAttribute("airlineName", this.getAirlineName());
			pageContext.setAttribute("departureDate", this.getDepartureDate());
			pageContext.setAttribute("departureGate", this.getDepartureGate());
			pageContext.setAttribute("destination", this.getDestination());
			pageContext.setAttribute("cost", this.getCost());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return EVAL_BODY_INCLUDE;
	}

}
