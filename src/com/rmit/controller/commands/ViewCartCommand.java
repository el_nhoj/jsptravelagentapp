package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.Cart;

public class ViewCartCommand extends AbstractCommand {

	public ViewCartCommand() {
		super();
	}
	
	@Override
	public void execute() {
		
		session = req.getSession(false);
		
		try {
			
			if (session.getAttribute("userID") != null) {
				
				// If session cart object expires, create new cart
				if (session.getAttribute("cart") == null) {
					session.setAttribute("cart", new Cart());
				}
				
				// Cast session cart to a Cart object
				Cart cart = (Cart) session.getAttribute("cart");
				
				// Calculate total
				session.setAttribute("total", cart.calculateTotal());
				session.setAttribute("cart", cart);
				req.getRequestDispatcher("viewCart.jsp").forward(req, resp);
				
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
			
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}		
	}
}
