package com.rmit.controller.commands;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import com.rmit.model.beans.Accommodation;
import com.rmit.model.beans.BookingConfirmationOrder;
import com.rmit.model.beans.Cart;
import com.rmit.model.beans.Customer;
import com.rmit.model.beans.Flight;
import com.rmit.model.database.ApplicationDatabase;

public class BookingConfirmationCommand extends AbstractCommand {

	public BookingConfirmationCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);
		
		ApplicationDatabase appDB = new ApplicationDatabase();
		
		try {

			if (session.getAttribute("userID") != null) {

				// Cast required session objects to usable bean objects
				Cart cart = (Cart) session.getAttribute("cart");
				Customer customer = (Customer) session.getAttribute("customer");
				double total = (double) session.getAttribute("total");

				String cartContents = "";

				// If cart contains Flights
				if (!cart.getFlightList().isEmpty()) {

					// Cast session flights to array
					List<Flight> flights = cart.getFlightList();

					for (Flight f : flights) {
						cartContents += f.getId() + " : " + f.getDestination()
								+ " : " + f.getAirlineName() + " : "
								+ f.getDepartureDate() + " : "
								+ f.getDepartureGate() + "_";
					}
				}

				// If cart contains Accommodations
				if (!cart.getAccommodationList().isEmpty()) {
					// Cast session accommodations to array
					List<Accommodation> accommodations = cart
							.getAccommodationList();

					for (Accommodation a : accommodations) {
						cartContents += a.getName() + " : " + a.getAddress()
								+ " : " + a.getLocation() + " : "
								+ a.getPhoneNumber() + "_";
					}
				}

				// Create booking confirmation for flights
				BookingConfirmationOrder bookingConfirmationOrder = appDB
						.createBookingConfirmation(customer, total,
								cartContents);

				if (bookingConfirmationOrder != null) {

					// Set updated customer information to customer object
					customer = appDB.getCustomerDetails(customer.getUsername());
					// Put updated customer as updated session variable
					session.setAttribute("customer", customer);

					// Unset session variables in memory
					session.removeAttribute("cart");
					session.removeAttribute("cartFlights");
					session.removeAttribute("cartAccommodations");
					session.removeAttribute("total");

					// Pass booking object to next page for display
					req.setAttribute("bookingConfirmationOrder",
							bookingConfirmationOrder);
					req.getRequestDispatcher("bookingSuccess.jsp").forward(req,
							resp);
				} else {
					// Failed to create booking confirmation
					req.setAttribute("error",
							"Your account does not have sufficient funds to complete the transaction.");
					req.getRequestDispatcher("checkout.jsp").forward(req, resp);
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}
