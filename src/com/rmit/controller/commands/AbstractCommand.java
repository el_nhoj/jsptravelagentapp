package com.rmit.controller.commands;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public abstract class AbstractCommand implements Command {

	protected HttpServletRequest req;
	protected HttpServletResponse resp;
	protected PrintWriter out;
	protected HttpSession session;

	@Override
	public void init(HttpServletRequest req, HttpServletResponse resp) {
		this.req = req;
		this.resp = resp;
		this.session = req.getSession();

		try {
			out = resp.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void forward(String uri) throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getRequestDispatcher(uri);
	    dispatcher.forward(req, resp);
	}

	@Override
	public abstract void execute();

}
