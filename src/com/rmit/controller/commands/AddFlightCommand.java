package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.Cart;
import com.rmit.model.beans.Flight;

public class AddFlightCommand extends AbstractCommand {

	public AddFlightCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {

			if (session.getAttribute("userID") != null) {

				// Check if flightIds were passed from the form
				if (req.getParameter("flightId") == null) {
					req.setAttribute("error", "No flights were selected!");
					req.getRequestDispatcher("bookFlight.jsp").forward(req,
							resp);
				} else {
					// Put flightId posted from form into String
					String flightId = req.getParameter("flightId");

					// If session cart object expires, create new cart
					if (session.getAttribute("cart") == null) {
						session.setAttribute("cart", new Cart());
					}

					// Cast session cart to a Cart object
					Cart cart = (Cart) session.getAttribute("cart");

					// Cast sessionFlights array to a Flight array object
					Flight[] sessionFlights = (Flight[]) session
							.getAttribute("sessionFlights");

					String destination = null;

					boolean isNotDuplicate = true;

					// Loop through both arrays to find a match by flightId
					for (Flight flight : sessionFlights) {
						if (flight.getId().equals(flightId)) {
							if (duplicateCheck(flight, cart)) {
								// If not duplicate, add to cart and assign its
								// destination to string
								cart.getFlightList().add(flight);
								destination = flight.getDestination();
								isNotDuplicate = true;
							} else {
								isNotDuplicate = false;
							}
						}
					}

					if (isNotDuplicate == true) {
						// Set processed Cart object back to session cart
						session.setAttribute("cart", cart);
						session.setAttribute("destination", destination);
						session.setAttribute("cartFlights",
								cart.getFlightList());
						
						req.setAttribute("msg", "Flight " + flightId + " was added successfully!");
						req.getRequestDispatcher("getAction?command=getAccommodation").forward(req, resp);
					} else {
						req.setAttribute("error",
								"Cannot add flight, already in cart!");
						req.getRequestDispatcher("bookFlight.jsp").forward(req,
								resp);
					}
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

	// Method to check for duplicate flights in cart
	public boolean duplicateCheck(Flight flight, Cart cart) {
		boolean isNotDuplicate = false;

		// If ID does not match, not a duplicate object
		if (!cart.getFlightList().contains(flight)) {
			isNotDuplicate = true;
		}

		return isNotDuplicate;
	}
}
