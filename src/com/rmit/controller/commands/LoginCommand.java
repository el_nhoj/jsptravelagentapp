package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.Cart;
import com.rmit.model.beans.Customer;
import com.rmit.model.database.ApplicationDatabase;

public class LoginCommand extends AbstractCommand {

	public LoginCommand() {
		super();
	}

	@Override
	public void execute() {

		ApplicationDatabase appDB = new ApplicationDatabase();

		try {
			String user = req.getParameter("username");
			String pass = req.getParameter("password");

			// Check form fields for customer credentials and set session if
			// valid
			if (appDB.validateUser(user, pass)) {

				session = req.getSession(true);

				// Get customer details in the form of an object
				Customer customer = appDB.getCustomerDetails(user);

				// Set session variables
				session.setAttribute("userID", user);
				session.setAttribute("customer", customer);
				session.setAttribute("cart", new Cart());

				req.getRequestDispatcher("makeBooking.jsp").forward(req, resp);

			} else {
				req.setAttribute("error",
						"Wrong username or password. Please try again.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}
