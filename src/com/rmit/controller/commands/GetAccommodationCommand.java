package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.Accommodation;
import com.rmit.model.database.ApplicationDatabase;

public class GetAccommodationCommand extends AbstractCommand {
	
	public GetAccommodationCommand() {
		super();
	}
	
	@Override
	public void execute() {
		
		session = req.getSession(false);
		
		ApplicationDatabase appDB = new ApplicationDatabase();
		
		try {	

			if (session.getAttribute("userID") != null) {
				
				String destination = (String) session.getAttribute("destination");
				
				Accommodation[] accommodations = appDB.getAccommodation(destination);
				
				session.setAttribute("sessionAccommodations", accommodations);
				
				req.getRequestDispatcher("bookAccommodation.jsp")
						.forward(req, resp);

			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
			
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
		
	}

}
