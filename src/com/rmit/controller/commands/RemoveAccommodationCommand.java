package com.rmit.controller.commands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import com.rmit.model.beans.Accommodation;
import com.rmit.model.beans.Cart;

public class RemoveAccommodationCommand extends AbstractCommand {

	public RemoveAccommodationCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {

			if (session.getAttribute("userID") != null) {

				// Check if accommodationIds were passed from the form
				if (req.getParameter("accommodationId") == null) {
					req.setAttribute("error", "No hotels were selected!");
					req.getRequestDispatcher("bookAccommodation.jsp").forward(
							req, resp);
				} else {
					// Retrieve accommodationId parameter
					String accommodationId = req
							.getParameter("accommodationId");

					// Cast session cart to a Cart object
					Cart cart = (Cart) session.getAttribute("cart");

					// Create temporary list
					List<Accommodation> temp = new ArrayList<Accommodation>();

					// Loop through original list and add the accommodations *not*
					// matching the accommodationId
					for (Accommodation acommodation : cart.getAccommodationList()) {
						if (!acommodation.getId().equals(accommodationId)) {
							temp.add(acommodation);
						}
					}
					
					// Set temp list as the new accommodationList
					cart.setAccommodationList(temp);
					
					// Set processed Cart object back to session cart
					session.setAttribute("cart", cart);
					req.setAttribute("msg", "Accommodation removed!");
					// Calculate total
					session.setAttribute("total", cart.calculateTotal());

					// Check if cart has no accommodations
					if (cart.getAccommodationList().isEmpty()) {
						session.removeAttribute("cartAccommodations");
					} else {
						session.setAttribute("cartAccommodations",
								cart.getAccommodationList());
					}
					req.getRequestDispatcher("viewCart.jsp").forward(req, resp);
				}

			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}
}
