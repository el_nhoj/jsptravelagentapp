package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class ConvertCurrencyCommand extends AbstractCommand {

	public ConvertCurrencyCommand() {
		super();
	}

	@Override
	public void execute() {

		try {

			String fromCurrency = req.getParameter("fromCurrency");
			String toCurrency = req.getParameter("toCurrency");

			try {

				// Set destination to a port that can be captured by tcpmon
				// System.setProperty("http.proxyHost", "localhost");
				// System.setProperty("http.proxyPort", "8080");

				SAXBuilder builder = new SAXBuilder();

				Document doc = builder
						.build("http://www.webservicex.net/CurrencyConvertor.asmx/ConversionRate?FromCurrency="
								+ fromCurrency
								+ "&ToCurrency="
								+ toCurrency
								+ "");

				StringBuffer output = new StringBuffer();
				output.append("1 " + fromCurrency + " converts to ");
				output.append(doc.getRootElement().getValue());
				output.append(" " + toCurrency);
				String convertedCurrency = output.toString();

				req.setAttribute("convertedCurrency", convertedCurrency);
				req.setAttribute("selectedList", fromCurrency);
				req.getRequestDispatcher("exchangeRate.jsp").forward(req, resp);

			} catch (JDOMException e) {
				e.printStackTrace();
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}
