package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

public class LogoutCommand extends AbstractCommand {
	
	public LogoutCommand() {
		super();
	}
	
	@Override
	public void execute() {
		try {
			session = req.getSession();
			
			// Destroy session and redirect to login page
			session.invalidate();
			
			req.setAttribute("error", "You have been logged out.");
			req.getRequestDispatcher("login.jsp").forward(req, resp);
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}
