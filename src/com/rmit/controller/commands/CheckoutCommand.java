package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.Cart;

public class CheckoutCommand extends AbstractCommand {

	public CheckoutCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {

			if (session.getAttribute("userID") != null) {

				// Cast session cart to a Cart object
				Cart cart = (Cart) session.getAttribute("cart");

				// Check if accommodations have a matching flight, if
				// accommodations
				// and flights in cart are not empty
				if (!cart.getAccommodationList().isEmpty()
						&& !cart.getFlightList().isEmpty()) {
					// Compare flight and accommodation cart sizes
					if(cart.getAccommodationList().size() == cart.getFlightList().size()){
						req.getRequestDispatcher("checkout.jsp").forward(req, resp);
					} else {
						req.setAttribute("error", "There are uneven flight/accommodation pairs!");
						req.getRequestDispatcher("viewCart.jsp").forward(req, resp);
					}					
				} else {
					req.setAttribute("error", "There are no matching flight/accommodation pairs!");
					req.getRequestDispatcher("viewCart.jsp").forward(req, resp);	
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}
