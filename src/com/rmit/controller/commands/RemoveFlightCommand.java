package com.rmit.controller.commands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import com.rmit.model.beans.Cart;
import com.rmit.model.beans.Flight;

public class RemoveFlightCommand extends AbstractCommand {

	public RemoveFlightCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {

			if (session.getAttribute("userID") != null) {
				// Check if flightIds were passed from the form
				if (req.getParameter("flightId") == null) {
					req.setAttribute("error", "No flights were selected!");
					req.getRequestDispatcher("bookFlight.jsp").forward(req,
							resp);
				} else {
					// Retrieve flightId parameter
					String flightId = req.getParameter("flightId");

					// Cast session cart to Cart object
					Cart cart = (Cart) session.getAttribute("cart");

					// Create temporary list
					List<Flight> temp = new ArrayList<Flight>();

					// Loop through original list and add the flights *not*
					// matching the flightId
					for (Flight f : cart.getFlightList()) {
						if (!f.getId().equals(flightId)) {
							temp.add(f);
						}
					}

					// Set temp list as the new flightList
					cart.setFlightList(temp);

					// Set processed Cart object back to session cart
					session.setAttribute("cart", cart);
					req.setAttribute("msg", "Flight removed!");
					// Calculate total
					session.setAttribute("total", cart.calculateTotal());

					// Check if cart has no flights
					if (cart.getFlightList().isEmpty()) {
						session.removeAttribute("cartFlights");
					} else {
						session.setAttribute("cartFlights",
								cart.getFlightList());
					}

					req.getRequestDispatcher("viewCart.jsp").forward(req, resp);
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}
}
