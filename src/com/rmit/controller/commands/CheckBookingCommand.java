package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.BookingConfirmationOrder;
import com.rmit.model.beans.Customer;
import com.rmit.model.database.ApplicationDatabase;

public class CheckBookingCommand extends AbstractCommand {

	public CheckBookingCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);
		
		ApplicationDatabase appDB = new ApplicationDatabase();
		
		try {
			if (session.getAttribute("userID") != null) {

				// Get booking confirmation ID from previous form
				String bookingId = req.getParameter("bcid");

				// Get customer object from session to do comparisons
				Customer customer = (Customer) session.getAttribute("customer");

				// Retrieve booking object based on booking confirmation ID
				// provided
				BookingConfirmationOrder bookingOrder = appDB
						.getBookingConfirmation(bookingId);

				// If booking confirmation object is returned
				if (bookingOrder != null) {

					// Check if this booking belongs to the customer
					if (customer.getId().equals(bookingOrder.getCustomerId())) {

						// Split cart contents into tokens
						String s = bookingOrder.getCartContents();
						String delimiter = "[_]";
						String[] tokens = s.split(delimiter);

						// Pass booking confirmation object to next page
						req.setAttribute("bookingOrder", bookingOrder);
						req.setAttribute("tokens", tokens);
						req.getRequestDispatcher("checkBooking.jsp").forward(
								req, resp);
					} else {
						req.setAttribute("error",
								"Booking Confirmation ID is invalid or does not exist.");
						req.getRequestDispatcher("makeBooking.jsp").forward(
								req, resp);
					}
				} else {
					req.setAttribute("error",
							"Booking Confirmation ID is invalid or does not exist.");
					req.getRequestDispatcher("makeBooking.jsp").forward(req,
							resp);
				}

			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}
