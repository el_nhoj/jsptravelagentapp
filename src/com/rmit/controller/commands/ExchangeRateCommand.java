package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

public class ExchangeRateCommand extends AbstractCommand {
	
	public ExchangeRateCommand() {
		super();
	}
	
	@Override
	public void execute() {
		
		try {
			String s = "USD:EUR:GBP:INR:CAD:NZD:JPY";
			String delimiter = "[:]";
			String[] tokens = s.split(delimiter);
			
			session.setAttribute("tokens", tokens);
			req.getRequestDispatcher("exchangeRate.jsp").forward(req, resp);
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}		
	}

}
