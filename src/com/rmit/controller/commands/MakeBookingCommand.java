package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

public class MakeBookingCommand extends AbstractCommand {

	public MakeBookingCommand() {
		super();
	}

	@Override
	public void execute() {
		try {			 
			req.getRequestDispatcher("makeBooking.jsp").forward(req, resp);
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}
}
