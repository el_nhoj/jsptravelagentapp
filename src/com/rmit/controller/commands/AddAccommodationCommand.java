package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.Accommodation;
import com.rmit.model.beans.Cart;

public class AddAccommodationCommand extends AbstractCommand {

	public AddAccommodationCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {

			if (session.getAttribute("userID") != null) {

				// Check if accommodationIds were passed from the form
				if (req.getParameter("accommodationId") == null) {
					req.setAttribute("error", "No hotels were selected!");
					req.getRequestDispatcher("bookAccommodation.jsp").forward(
							req, resp);
				} else {

					// Put accommodationIds posted from form into String array
					String[] accommodationIds = req
							.getParameterValues("accommodationId");

					// If session cart object expires, create new cart
					if (session.getAttribute("cart") == null) {
						session.setAttribute("cart", new Cart());
					}

					// Cast session cart to a Cart object
					Cart cart = (Cart) session.getAttribute("cart");

					// Cast sessionAccommodation array to a Accommodation array
					// object
					Accommodation[] sessionAccommodations = (Accommodation[]) session
							.getAttribute("sessionAccommodations");

					boolean isNotDuplicate = true;
					String hotelName = null;

					// Loop through both arrays to find a match by
					// accommodationId
					for (Accommodation accommodation : sessionAccommodations) {
						for (String id : accommodationIds) {
							if (accommodation.getId().equals(id)) {

								if (checkDuplicate(accommodation, cart)) {
									// If not duplicate, add to cart and assign
									// its
									// name to string
									cart.getAccommodationList().add(
											accommodation);
									hotelName = accommodation.getName();
									isNotDuplicate = true;
								} else {
									isNotDuplicate = false;
								}

							}
						}
					}

					if (isNotDuplicate == true) {
						// Set processed Cart object back to session cart
						session.setAttribute("cart", cart);
						req.setAttribute("msg", "Hotel " + hotelName
								+ " was added successfully!");
						// Calculate total
						session.setAttribute("total", cart.calculateTotal());

						session.setAttribute("cartAccommodations",
								cart.getAccommodationList());
						req.getRequestDispatcher("viewCart.jsp").forward(req,
								resp);
					} else {
						req.setAttribute("error",
								"Cannot add accommodation, already in cart!");
						req.getRequestDispatcher("bookAccommodation.jsp")
								.forward(req, resp);
					}
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}

		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}

	}

	// Method to check for duplicate flights in cart
	public boolean checkDuplicate(Accommodation accommodation, Cart cart) {
		boolean isNotDuplicate = false;

		// If ID does not match, not a duplicate object
		if (!cart.getAccommodationList().contains(accommodation)) {
			isNotDuplicate = true;
		}

		return isNotDuplicate;
	}
}
