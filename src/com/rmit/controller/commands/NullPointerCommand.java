package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

public class NullPointerCommand extends AbstractCommand {
	
	public NullPointerCommand() {
		super();
	}
	
	@Override
	public void execute() {
		try {
			req.getRequestDispatcher("/errorHandling/error404.jsp").forward(req, resp);
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
		
	}

}
