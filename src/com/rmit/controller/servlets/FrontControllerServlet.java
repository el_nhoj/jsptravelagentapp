package com.rmit.controller.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rmit.controller.commands.Command;

@WebServlet("/getAction")
public class FrontControllerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public FrontControllerServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/html");

		// Get correct command based on what 'command' parameter was passed
		Command command = getCommand(req.getParameter("command"));
		command.init(req, resp);
		command.execute();
	}

	// Method to process and return a command class based on given string
	private Command getCommand(String commandString) {
		Class<?> commandClass = null;
		Command command = null;
		String className = "";

		try {
			if (commandString != null) {
				switch (commandString) {
				case "addAccommodation":
					className = "com.rmit.controller.commands.AddAccommodationCommand";
					break;
				case "addFlight":
					className = "com.rmit.controller.commands.AddFlightCommand";
					break;
				case "bookFlight":
					className = "com.rmit.controller.commands.BookFlightCommand";
					break;
				case "bookingConfirmation":
					className = "com.rmit.controller.commands.BookingConfirmationCommand";
					break;
				case "checkBooking":
					className = "com.rmit.controller.commands.CheckBookingCommand";
					break;
				case "checkout":
					className = "com.rmit.controller.commands.CheckoutCommand";
					break;
				case "convertCurrency":
					className = "com.rmit.controller.commands.ConvertCurrencyCommand";
					break;
				case "exchangeRate":
					className = "com.rmit.controller.commands.ExchangeRateCommand";
					break;
				case "getAccommodation":
					className = "com.rmit.controller.commands.GetAccommodationCommand";
					break;
				case "login":
					className = "com.rmit.controller.commands.LoginCommand";
					break;
				case "logout":
					className = "com.rmit.controller.commands.LogoutCommand";
					break;
				case "makeBooking":
					className = "com.rmit.controller.commands.MakeBookingCommand";
					break;
				case "removeAccommodation":
					className = "com.rmit.controller.commands.RemoveAccommodationCommand";
					break;
				case "removeFlight":
					className = "com.rmit.controller.commands.RemoveFlightCommand";
					break;
				case "viewCart":
					className = "com.rmit.controller.commands.ViewCartCommand";
					break;
				default:
					className = "com.rmit.controller.commands.NullPointerCommand";
					break;
				}
			} else {
				className = "com.rmit.controller.commands.NullPointerCommand";
			}

			// Check: Print out selected class name in console
			System.out.println("ClassName selected: " + className);

			// Instantiate the appropriate class
			commandClass = Class.forName(className);
			command = (Command) commandClass.newInstance();

		} catch (ClassNotFoundException cnfe) {
			System.out.println("ClassNotFoundException: Invalid commandString");
			return null;
		} catch (InstantiationException ie) {
			System.out.println("InstantiationException: Invalid commandString");
			return null;
		} catch (IllegalAccessException iae) {
			System.out.println("IllegalAccessException: Invalid commandString");
			return null;
		}

		return command;
	}
}
