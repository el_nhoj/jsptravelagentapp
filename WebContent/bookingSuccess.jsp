<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<h1>Booking Successful</h1>
<p>Thank you! Your holiday bookings have been confirmed and your
	payment has been successfully processed.</p>
<h2>Booking Confirmation ID</h2>
<p>Please retain the following booking confirmation ID for your own personal records.</p>
<table>
	<tr>
		<td class="bold"><c:out value="${bookingConfirmationOrder.id}" /></td>
	</tr>
</table>
<p>Do enjoy your vacation and we hope you will choose Web Travel
	again for all your future flight and accommodation needs!</p>
<p><a href="getAction?command=makeBooking">&raquo; Return</a></p>
<%@ include file="/includes/footer.html"%>