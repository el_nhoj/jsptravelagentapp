<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isErrorPage="true" %>
<%@ include file="/includes/header.jsp"%>
<h1>Application Error</h1>
<p>Oops...Something broke and an application-wide error has occurred.</p>
<pre>
	<% exception.printStackTrace(response.getWriter()); %>
</pre>
<%@ include file="/includes/footer.html"%>