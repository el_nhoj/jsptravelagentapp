<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h1>Check Booking</h1>
<p>Here is the booking information returned from the ID you have provided.</p>
<h2>Booking Confirmation ID:</h2>
<p class="bold"><c:out value="${bookingOrder.id}" /></p>
<table>
	<c:forEach var="list" items="${tokens}">
		<tr>
			<td>${list}</td>
		</tr>
	</c:forEach>
</table>
<br />
<p>
	<a href="getAction?command=makeBooking">&raquo; Return</a>
</p>
<%@ include file="/includes/footer.html"%>