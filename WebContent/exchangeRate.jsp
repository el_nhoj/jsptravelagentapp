<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h1>Exchange Rate</h1>
<p>Customers looking to book internationally may want to check the
	current exchange rate with our national currency (AUD) before
	processing payment.</p> 
<p>Please use our simple currency converter below:</p>
<h2>${convertedCurrency}</h2>
<form action="getAction" method="post" name="convertCurrencyForm">
	<label class="bold">From: </label>
	<select name="fromCurrency">
		<c:forEach var="list" items="${tokens}">
			<c:choose>
				<c:when test="${selectedList eq list}" >
					<option value="${list}" selected="${list}">${list}</option>
				</c:when>
				<c:otherwise>
					<option value="${list}">${list}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>		
	</select>
	<label class="bold">To: </label>
	<select name="toCurrency">
		<option value="AUD" selected="selected">AUD</option>
	</select>
	<button type="submit" name="command" value="convertCurrency">Convert</button>
</form>
<%@ include file="/includes/footer.html"%>