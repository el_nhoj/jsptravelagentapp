<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp" %>
	<h1>Login</h1>
	<p>Please enter your customer username and password.</p>
	<p class="error">${error}</p>
	<form action="getAction" method="POST" name="loginForm">
		<label class="bold">Username: </label>
		<input type="text" id="username" name="username" type="text" />
		<br/>
		<br/>
		<label class="bold">Password: </label>
		<input type="password" id="password" name="password" type="text" />
		<br/>
		<br/>
		<button type="submit" name="command" value="login">Login</button>		
		<br/>
	</form>	
<%@ include file="/includes/footer.html" %>