<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fCustTag" uri="/WEB-INF/customTags/FlightTag.tld"%>
<%@ taglib prefix="aCustTag" uri="/WEB-INF/customTags/AccommodationTag.tld"%>
<h1>Your Shopping Cart</h1>
<p class="success">${msg}</p>
<p class="error">${error}</p>
<c:if test="${cartFlights != null}">
	<h2>Flights</h2>
	<table>
		<tr>
			<th>Flight ID</th>
			<th>Destination</th>
			<th>Airline</th>
			<th>Departure Gate</th>
			<th>Departure Date</th>
			<th>Price</th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach var="list" items="${cartFlights}">
			<fCustTag:flightDetails airlineName="${list.airlineName}"
				id="${list.id}" departureDate="${list.departureDate}"
				cost="${list.cost}" departureGate="${list.departureGate}"
				destination="${list.destination}">
				<tr>
					<td><c:out value="${id}" /></td>
					<td><c:out value="${destination}" /></td>
					<td><c:out value="${airlineName}" /></td>
					<td><c:out value="${departureGate}" /></td>
					<td><c:out value="${departureDate}" /></td>
					<td><fmt:formatNumber value="${cost}" type="currency" /></td>
					<td><a
						href="getAction?command=removeFlight&flightId=${id}"
						class="bold">Remove</a></td>
				</tr>
			</fCustTag:flightDetails>
		</c:forEach>
	</table>
	<br />
</c:if>
<c:if test="${cartAccommodations != null}">
	<h2>Accommodations</h2>
	<table>
		<tr>
			<th>Hotel Name</th>
			<th>Address</th>
			<th>Location</th>
			<th>Phone Number</th>
			<th>Price</th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach var="list2" items="${cartAccommodations}">
			<aCustTag:accommodationDetails location="${list2.location}"
				address="${list2.address}" phoneNumber="${list2.phoneNumber}"
				name="${list2.name}" cost="${list2.cost}" id="${list2.id}">
				<tr>
					<td><c:out value="${name}" /></td>
					<td><c:out value="${address}" /></td>
					<td><c:out value="${location}" /></td>
					<td><c:out value="${phoneNumber}" /></td>
					<td><fmt:formatNumber value="${cost}" type="currency" /></td>
					<td><a
						href="getAction?command=removeAccommodation&accommodationId=${id}"
						class="bold">Remove</a></td>
				</tr>
			</aCustTag:accommodationDetails>
		</c:forEach>
	</table>
</c:if>
<p>
	<span class="bold">Total: </span>
	<c:choose>
		<c:when test="${total != null}">
			<fmt:formatNumber value="${total}" type="currency" />
		</c:when>
		<c:otherwise>
			&#36;0.00
		</c:otherwise>
	</c:choose>	
</p>
<div class="left">
	<p><a href="getAction?command=makeBooking">&laquo; Make Another Booking</a></p>
</div>
<div class="right">
	<c:choose>
		<c:when test="${(cartFlights != null) || (cartAccommodations != null)}">
			<p><a href="getAction?command=checkout">Checkout &raquo;</a></p>
		</c:when>
		<c:otherwise>
			<p>&nbsp;</p>
		</c:otherwise>	
	</c:choose>		
</div>
<%@ include file="/includes/footer.html"%>