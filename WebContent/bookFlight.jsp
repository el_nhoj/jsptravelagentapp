<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custTag" uri="/WEB-INF/customTags/FlightTag.tld"%>
<h1>Book Flights</h1>
<p class="error">${error}</p>
<p>Here is a list of available flights:</p>
<form action="getAction" method="post" name="addFlightForm">
	<table>
		<tr>
			<th>&nbsp;</th>
			<th>Flight ID</th>
			<th>Destination</th>
			<th>Airline</th>
			<th>Departure Gate</th>
			<th>Departure Date</th>
			<th>Price</th>
		</tr>
		<c:forEach var="list" items="${sessionFlights}">
			<custTag:flightDetails airlineName="${list.airlineName}"
				id="${list.id}" departureDate="${list.departureDate}"
				cost="${list.cost}" departureGate="${list.departureGate}"
				destination="${list.destination}">
				<tr>
					<td><input type="radio" name="flightId" value="${id}" /></td>
					<td><c:out value="${id}" /></td>
					<td><c:out value="${destination}" /></td>
					<td><c:out value="${airlineName}" /></td>
					<td><c:out value="${departureGate}" /></td>
					<td><c:out value="${departureDate}" /></td>
					<td><fmt:formatNumber value="${cost}" 
            type="currency" /></td>
				</tr>
			</custTag:flightDetails>
		</c:forEach>
	</table>
	<button type="submit" name="command" value="addFlight">Book Now</button>
</form>

<%@ include file="/includes/footer.html"%>