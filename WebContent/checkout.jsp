<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fCustTag" uri="/WEB-INF/customTags/FlightTag.tld"%>
<%@ taglib prefix="aCustTag" uri="/WEB-INF/customTags/AccommodationTag.tld"%>
<h1>Checkout</h1>
<p class="error">${error}</p>
<p>Please check your customer details are correct before confirming your booking.</p>
<h2>Customer Details</h2>
<table>
	<tr>
		<td><span class="bold">Customer ID: </span></td>
		<td><c:out value="${customer.id}" /></td>
	</tr>
	<tr>
		<td><span class="bold">Full Name: </span></td>
		<td><c:out value="${customer.name}" /></td>
	</tr>
	<tr>
		<td><span class="bold">Residential Address: </span></td>
		<td><c:out value="${customer.address}" /></td>
	</tr>
	<tr>
		<td><span class="bold">Balance: </span></td>
		<td><fmt:formatNumber value="${customer.balance}" type="currency" /></td>
	</tr>
</table>
<br />
<c:if test="${cartFlights != null}">
	<h2>Flights</h2>
	<table>
		<tr>
			<th>Flight ID</th>
			<th>Destination</th>
			<th>Airline</th>
			<th>Departure Gate</th>
			<th>Departure Date</th>
			<th>Price</th>
		</tr>
		<c:forEach var="list" items="${cartFlights}">
			<fCustTag:flightDetails airlineName="${list.airlineName}"
				id="${list.id}" departureDate="${list.departureDate}"
				cost="${list.cost}" departureGate="${list.departureGate}"
				destination="${list.destination}">
				<tr>
					<td><c:out value="${id}" /></td>
					<td><c:out value="${destination}" /></td>
					<td><c:out value="${airlineName}" /></td>
					<td><c:out value="${departureGate}" /></td>
					<td><c:out value="${departureDate}" /></td>
					<td><fmt:formatNumber value="${cost}" type="currency" /></td>
				</tr>
			</fCustTag:flightDetails>
		</c:forEach>
	</table>
	<br />
</c:if>
<c:if test="${cartAccommodations != null}">
	<h2>Accommodations</h2>
	<table>
		<tr>
			<th>Hotel Name</th>
			<th>Address</th>
			<th>Location</th>
			<th>Phone Number</th>
			<th>Price</th>
		</tr>
		<c:forEach var="list2" items="${cartAccommodations}">
			<aCustTag:accommodationDetails location="${list2.location}"
				address="${list2.address}" phoneNumber="${list2.phoneNumber}"
				name="${list2.name}" cost="${list2.cost}" id="${list2.id}">
				<tr>
					<td><c:out value="${name}" /></td>
					<td><c:out value="${address}" /></td>
					<td><c:out value="${location}" /></td>
					<td><c:out value="${phoneNumber}" /></td>
					<td><fmt:formatNumber value="${cost}" type="currency" /></td>
				</tr>
			</aCustTag:accommodationDetails>
		</c:forEach>
	</table>
</c:if>
<h2><span class="bold">Total: </span><fmt:formatNumber value="${total}" type="currency" /></h2>
<br/>
<div class="left">
	<p><a href="getAction?command=viewCart">&laquo; Back to Cart</a></p>
</div>
<div class="right">
	<p><a href="getAction?command=bookingConfirmation">Confirm Bookings &raquo;</a></p>
</div>
<%@ include file="/includes/footer.html"%>