<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h1>Make a Booking</h1>
<c:if test="${sessionScope.userID != null}">
	<div class="left">
		<p>
			Welcome back, <span class="bold">${sessionScope.userID}</span>.
		</p>
	</div>
	<div class="right">
		<form action="getAction" method="post" name="logoutForm">
			<button type="submit" name="command" value="logout">Logout</button>
		</form>
	</div>
</c:if>
<h2 class="clear">Book Flights</h2>
<p><a href="getAction?command=bookFlight">&raquo; View Current Available Flights</a></p>
<h2>Check Bookings</h2>
<p class="error">${error}</p> 
<form action="getAction" method="post" name="BCNCheckForm">
	<label>Please enter a Booking Confirmation ID:</label>
	<br/>
	<input type="text" name="bcid" size="40">
	<button type="submit" name="command" value="checkBooking">Search ID</button> 
</form>
<%@ include file="/includes/footer.html"%>