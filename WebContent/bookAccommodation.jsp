<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/includes/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="custTag"
	uri="/WEB-INF/customTags/AccommodationTag.tld"%>
<h1>Book Accommodations</h1>
<p class="error">${error}</p>
<p class="success">${msg}</p>
<c:if test="${sessionAccommodations != null}">
	<p>Here is a list of available hotels:</p>
	<form action="getAction" method="post" name="addAccommForm">
		<table>
			<tr>
				<th>&nbsp;</th>
				<th>Hotel Name</th>
				<th>Address</th>
				<th>Location</th>
				<th>Phone Number</th>
				<th>Price</th>
			</tr>
			<c:forEach var="list" items="${sessionAccommodations}">
				<custTag:accommodationDetails id="${list.id}" name="${list.name}"
					address="${list.address}" location="${list.location}"
					phoneNumber="${list.phoneNumber}" cost="${list.cost}">
					<tr>
						<td><input type="radio" name="accommodationId"
							value="${id}" /></td>
						<td><c:out value="${name}" /></td>
						<td><c:out value="${address}" /></td>
						<td><c:out value="${location}" /></td>
						<td><c:out value="${phoneNumber}" /></td>
						<td><fmt:formatNumber value="${cost}" type="currency" /></td>
					</tr>
				</custTag:accommodationDetails>
			</c:forEach>
		</table>
		<button type="submit" name="command" value="addAccommodation">Book Now</button>
	</form>
</c:if>
<%@ include file="/includes/footer.html"%>