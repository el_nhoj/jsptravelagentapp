<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>WebTravel Booking Services - Your No.1 Booking Service
	for Flights and Accommodations!</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href='http://fonts.googleapis.com/css?family=Oswald:400,700'
	rel='stylesheet' type='text/css'>
</head>

<body>
	<div id="wrapper">
		<div id="header-wrap">
			<header id="header">
				<img src="images/logo.png" />
				<h1>Your No.1 Booking Service for Flights and Accommodations!</h1>
			</header>
		</div>
		<div id="nav-wrap">
			<nav id="nav">
				<ul>
					<li><a href="getAction?command=makeBooking">Make a Booking</a></li>
					<li><a href="getAction?command=viewCart">View Cart</a></li>
					<li><a href="getAction?command=exchangeRate">Exchange
							Rate</a></li>		
					<core:choose>
						<core:when test="${sessionScope.userID != null}">
							<li><a href="getAction?command=logout">Logout</a></li>
						</core:when>
						<core:otherwise>
							<li><a href="/3401071_ws2">Login</a></li>
						</core:otherwise>
					</core:choose>
				</ul>
			</nav>
		</div>
		<div id="main-wrap">
			<div id="main">